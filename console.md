# Поиск директории логов
```console
remote@hostname:~$ pg_lsclusters | grep 'main' | awk '{print $(NF-1) "/" $(NF)}' | xargs dirname
```
---
```
/var/lib/postgresql/16/main/log
```
# Проверка существования файла /var/lib/postgresql/16/main/log
```console
remote@hostname:~$ die() { rc=$?; (( rc != 1 )) && set -- ""; (( $# )) && printf '%s\n' "$*"; return $(( rc == 0 ? 1 : rc )); }
remote@hostname:~$ test -d /var/lib/postgresql/16/main/log && printf '%s\n' "директория существует" || die "директории не существует"
```
---
```
директория существует
```
# проверка, что директория /etc/sudoers.d пустая
```console
remote@hostname:~$ die() { rc=$?; (( rc != 1 )) && set -- ""; (( $# )) && printf '%s\n' "$*"; return $(( rc == 0 ? 1 : rc )); }
remote@hostname:~$ [[ -z $(ls -A /var/lib/postgresql/16/main/log) ]] && printf '%s\n' "директория пустая" || die "директория не пустая"
```
---
```
директория не пустая
```
# Анализ событий модификации таблицы - INSERT
```console
remote@hostname:~$ #!/bin bash
remote@hostname:~$ LOG_DIR="/var/lib/postgresql/16/main/log"
remote@hostname:~$ # Инициализация переменных
remote@hostname:~$ total_events=0
remote@hostname:~$ success_events=0
remote@hostname:~$ failure_events=0
remote@hostname:~$ first_event=""
remote@hostname:~$ last_event=""
remote@hostname:~$ declare -A subject_ids
remote@hostname:~$ # Инициализация счетчиков для обязательных полей
remote@hostname:~$ date_time_count=0
remote@hostname:~$ process_id_count=0
remote@hostname:~$ subject_id_count=0
remote@hostname:~$ execution_result_count=0
remote@hostname:~$ # Массив для хранения строк с отсутствующими полями
remote@hostname:~$ missing_fields_lines=()
remote@hostname:~$ # Обработка лог-файлов
remote@hostname:~$ while IFS=',' read -r date time db pid _ _ _ _ _ result rest; do
remote@hostname:~$     if [[ "$rest" == *"statement: INSERT FROM table"* ]]; then
remote@hostname:~$         ((total_events++))
remote@hostname:~$         
remote@hostname:~$         event_datetime="$date $time"
remote@hostname:~$         
remote@hostname:~$         if [[ -z "$first_event" || "$event_datetime" < "$first_event" ]]; then
remote@hostname:~$             first_event="$event_datetime"
remote@hostname:~$         fi
remote@hostname:~$         
remote@hostname:~$         if [[ -z "$last_event" || "$event_datetime" > "$last_event" ]]; then
remote@hostname:~$             last_event="$event_datetime"
remote@hostname:~$         fi
remote@hostname:~$         
remote@hostname:~$         if [[ "$result" == "LOG" || "$result" == "0" || -z "$result" ]]; then
remote@hostname:~$             ((success_events++))
remote@hostname:~$         else
remote@hostname:~$             ((failure_events++))
remote@hostname:~$         fi
remote@hostname:~$         
remote@hostname:~$         # Increment the count for this database
remote@hostname:~$         if [[ -n "$db" ]]; then
remote@hostname:~$             subject_ids["$db"]=$((${subject_ids["$db"]:=0} + 1))
remote@hostname:~$         fi
remote@hostname:~$         # Проверка каждого обязательного поля
remote@hostname:~$         missing_fields=""
remote@hostname:~$         [[ "$date" != "" && "$time" != "" ]] && ((date_time_count++)) || missing_fields+="Date/Time "
remote@hostname:~$         [[ "$pid" != "" ]] && ((process_id_count++)) || missing_fields+="ProcessID "
remote@hostname:~$         [[ "$db" != "" ]] && ((subject_id_count++)) || missing_fields+="SubjectID "
remote@hostname:~$         [[ "$result" != "" ]] && ((execution_result_count++)) || missing_fields+="ExecutionResult "
remote@hostname:~$         # Если есть отсутствующие поля, добавляем строку в массив
remote@hostname:~$         log_line="$date $time $db"
remote@hostname:~$         if [[ -n "$missing_fields" ]]; then
remote@hostname:~$             missing_fields_lines+=("$log_line - Missing: $missing_fields")
remote@hostname:~$         fi
remote@hostname:~$     fi
remote@hostname:~$ done < <(grep -h "statement: INSERT FROM table" "$LOG_DIR"/*.log)
remote@hostname:~$ # Вывод результатов
remote@hostname:~$ echo "#### Сводка событий INSERT"
remote@hostname:~$ echo
remote@hostname:~$ echo "**Диапазон дат:** $first_event до $last_event"
remote@hostname:~$ echo "**Всего событий:** $total_events"
remote@hostname:~$ if [ $total_events -eq 0 ]; then
remote@hostname:~$     echo "**Процент успешных:** N/A (0 успешных, 0 неудачных)"
remote@hostname:~$ else
remote@hostname:~$     echo "**Процент успешных:** $((success_events * 100 / total_events))% ($success_events успешных, $failure_events неудачных)"
remote@hostname:~$ fi
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Подробная разбивка"
remote@hostname:~$ echo
remote@hostname:~$ echo "1. **Временная шкала событий**"
remote@hostname:~$ echo "   - Первое событие: $first_event"
remote@hostname:~$ echo "   - Последнее событие: $last_event"
remote@hostname:~$ echo
remote@hostname:~$ echo "2. **Идентификаторы субъектов**"
remote@hostname:~$ for db in "${!subject_ids[@]}"; do
remote@hostname:~$     echo "   - $db: ${subject_ids[$db]}"
remote@hostname:~$ done
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Анализ"
remote@hostname:~$ echo
remote@hostname:~$ if [ $total_events -eq 0 ]; then
remote@hostname:~$     echo "- **Согласованность:** N/A (нет событий)"
remote@hostname:~$ else
remote@hostname:~$     echo "- **Согласованность:** $((success_events * 100 / total_events))% операций INSERT были успешными."
remote@hostname:~$ fi
remote@hostname:~$ echo "- **Частота:** События происходили в течение периода от первого до последнего события."
remote@hostname:~$ echo "- **Использование базы данных:** Операции INSERT выполнялись на $(echo "${!subject_ids[@]}" | wc -w) базе(ах) данных."
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Проверка обязательной информации"
remote@hostname:~$ mandatory_fields=0
remote@hostname:~$ mandatory_fields_present=0
remote@hostname:~$ [[ $date_time_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ [[ $process_id_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ [[ $subject_id_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ [[ $execution_result_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ echo "Дата и время: $([ $date_time_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $date_time_count)"
remote@hostname:~$ echo "Идентификатор процесса: $([ $process_id_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $process_id_count)"
remote@hostname:~$ echo "Идентификатор субъекта: $([ $subject_id_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $subject_id_count)"
remote@hostname:~$ echo "Результат выполнения: $([ $execution_result_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $execution_result_count)"
remote@hostname:~$ echo
remote@hostname:~$ mandatory_fields=4
remote@hostname:~$ echo "Всего Да: $mandatory_fields_present"
remote@hostname:~$ echo "Всего Нет: $((mandatory_fields - mandatory_fields_present))"
remote@hostname:~$ echo
remote@hostname:~$ echo "Все обязательные поля присутствуют: $([ $mandatory_fields_present -eq $mandatory_fields ] && echo "Да" || echo "Нет")"
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Строки с отсутствующими полями"
remote@hostname:~$ if [ $(echo "${missing_fields_lines[@]}" | wc -w) -eq 0 ]; then
remote@hostname:~$     echo "Нет строк с отсутствующими полями."
remote@hostname:~$ else
remote@hostname:~$     printf '%s\n' "${missing_fields_lines[@]}"
remote@hostname:~$ fi
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Ошибки в логах"
remote@hostname:~$ echo
remote@hostname:~$ # grep -h "ERROR" "$LOG_DIR"/*.log | tail -n 10
```
---
```
#### Сводка событий INSERT
**Диапазон дат:** 2024-07-20 22:13:52 MSK postgres до 2024-09-19 21:49:21 MSK postgres
**Всего событий:** 1460
**Процент успешных:** 100% (1460 успешных, 0 неудачных)
#### Подробная разбивка
1. **Временная шкала событий**
   - Первое событие: 2024-07-20 22:13:52 MSK postgres
   - Последнее событие: 2024-09-19 21:49:21 MSK postgres
2. **Идентификаторы субъектов**
   - database: 1460
#### Анализ
- **Согласованность:** 100% операций INSERT были успешными.
- **Частота:** События происходили в течение периода от первого до последнего события.
- **Использование базы данных:** Операции INSERT выполнялись на 1 базе(ах) данных.
#### Проверка обязательной информации
Дата и время: Да (Количество: 1460)
Идентификатор процесса: Да (Количество: 1460)
Идентификатор субъекта: Да (Количество: 1460)
Результат выполнения: Да (Количество: 1460)
Всего Да: 4
Всего Нет: 0
Все обязательные поля присутствуют: Да
#### Строки с отсутствующими полями
2024-07-21 18:37:51 MSK postgres database - Missing: Date/Time ProcessID SubjectID ExecutionResult 
#### Ошибки в логах
```
# Анализ событий модификации таблицы - DELETE
```console
remote@hostname:~$ #!/bin bash
remote@hostname:~$ LOG_DIR="/var/lib/postgresql/16/main/log"
remote@hostname:~$ # Инициализация переменных
remote@hostname:~$ total_events=0
remote@hostname:~$ success_events=0
remote@hostname:~$ failure_events=0
remote@hostname:~$ first_event=""
remote@hostname:~$ last_event=""
remote@hostname:~$ declare -A subject_ids
remote@hostname:~$ # Инициализация счетчиков для обязательных полей
remote@hostname:~$ date_time_count=0
remote@hostname:~$ process_id_count=0
remote@hostname:~$ subject_id_count=0
remote@hostname:~$ execution_result_count=0
remote@hostname:~$ # Массив для хранения строк с отсутствующими полями
remote@hostname:~$ missing_fields_lines=()
remote@hostname:~$ # Обработка лог-файлов
remote@hostname:~$ while IFS=',' read -r date time db pid _ _ _ _ _ result rest; do
remote@hostname:~$     if [[ "$rest" == *"statement: DELETE FROM table"* ]]; then
remote@hostname:~$         ((total_events++))
remote@hostname:~$         
remote@hostname:~$         event_datetime="$date $time"
remote@hostname:~$         
remote@hostname:~$         if [[ -z "$first_event" || "$event_datetime" < "$first_event" ]]; then
remote@hostname:~$             first_event="$event_datetime"
remote@hostname:~$         fi
remote@hostname:~$         
remote@hostname:~$         if [[ -z "$last_event" || "$event_datetime" > "$last_event" ]]; then
remote@hostname:~$             last_event="$event_datetime"
remote@hostname:~$         fi
remote@hostname:~$         
remote@hostname:~$         if [[ "$result" == "LOG" || "$result" == "0" || -z "$result" ]]; then
remote@hostname:~$             ((success_events++))
remote@hostname:~$         else
remote@hostname:~$             ((failure_events++))
remote@hostname:~$         fi
remote@hostname:~$         
remote@hostname:~$         # Increment the count for this database
remote@hostname:~$         if [[ -n "$db" ]]; then
remote@hostname:~$             subject_ids["$db"]=$((${subject_ids["$db"]:=0} + 1))
remote@hostname:~$         fi
remote@hostname:~$         # Проверка каждого обязательного поля
remote@hostname:~$         missing_fields=""
remote@hostname:~$         [[ "$date" != "" && "$time" != "" ]] && ((date_time_count++)) || missing_fields+="Date/Time "
remote@hostname:~$         [[ "$pid" != "" ]] && ((process_id_count++)) || missing_fields+="ProcessID "
remote@hostname:~$         [[ "$db" != "" ]] && ((subject_id_count++)) || missing_fields+="SubjectID "
remote@hostname:~$         [[ "$result" != "" ]] && ((execution_result_count++)) || missing_fields+="ExecutionResult "
remote@hostname:~$         # Если есть отсутствующие поля, добавляем строку в массив
remote@hostname:~$         log_line="$date $time $db"
remote@hostname:~$         if [[ -n "$missing_fields" ]]; then
remote@hostname:~$             missing_fields_lines+=("$log_line - Missing: $missing_fields")
remote@hostname:~$         fi
remote@hostname:~$     fi
remote@hostname:~$ done < <(grep -h "statement: DELETE FROM table" "$LOG_DIR"/*.log)
remote@hostname:~$ # Вывод результатов
remote@hostname:~$ echo "#### Сводка событий DELETE"
remote@hostname:~$ echo
remote@hostname:~$ echo "**Диапазон дат:** $first_event до $last_event"
remote@hostname:~$ echo "**Всего событий:** $total_events"
remote@hostname:~$ if [ $total_events -eq 0 ]; then
remote@hostname:~$     echo "**Процент успешных:** N/A (0 успешных, 0 неудачных)"
remote@hostname:~$ else
remote@hostname:~$     echo "**Процент успешных:** $((success_events * 100 / total_events))% ($success_events успешных, $failure_events неудачных)"
remote@hostname:~$ fi
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Подробная разбивка"
remote@hostname:~$ echo
remote@hostname:~$ echo "1. **Временная шкала событий**"
remote@hostname:~$ echo "   - Первое событие: $first_event"
remote@hostname:~$ echo "   - Последнее событие: $last_event"
remote@hostname:~$ echo
remote@hostname:~$ echo "2. **Идентификаторы субъектов**"
remote@hostname:~$ for db in "${!subject_ids[@]}"; do
remote@hostname:~$     echo "   - $db: ${subject_ids[$db]}"
remote@hostname:~$ done
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Анализ"
remote@hostname:~$ echo
remote@hostname:~$ if [ $total_events -eq 0 ]; then
remote@hostname:~$     echo "- **Согласованность:** N/A (нет событий)"
remote@hostname:~$ else
remote@hostname:~$     echo "- **Согласованность:** $((success_events * 100 / total_events))% операций DELETE были успешными."
remote@hostname:~$ fi
remote@hostname:~$ echo "- **Частота:** События происходили в течение периода от первого до последнего события."
remote@hostname:~$ echo "- **Использование базы данных:** Операции DELETE выполнялись на $(echo "${!subject_ids[@]}" | wc -w) базе(ах) данных."
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Проверка обязательной информации"
remote@hostname:~$ mandatory_fields=0
remote@hostname:~$ mandatory_fields_present=0
remote@hostname:~$ [[ $date_time_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ [[ $process_id_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ [[ $subject_id_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ [[ $execution_result_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ echo "Дата и время: $([ $date_time_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $date_time_count)"
remote@hostname:~$ echo "Идентификатор процесса: $([ $process_id_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $process_id_count)"
remote@hostname:~$ echo "Идентификатор субъекта: $([ $subject_id_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $subject_id_count)"
remote@hostname:~$ echo "Результат выполнения: $([ $execution_result_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $execution_result_count)"
remote@hostname:~$ echo
remote@hostname:~$ mandatory_fields=4
remote@hostname:~$ echo "Всего Да: $mandatory_fields_present"
remote@hostname:~$ echo "Всего Нет: $((mandatory_fields - mandatory_fields_present))"
remote@hostname:~$ echo
remote@hostname:~$ echo "Все обязательные поля присутствуют: $([ $mandatory_fields_present -eq $mandatory_fields ] && echo "Да" || echo "Нет")"
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Строки с отсутствующими полями"
remote@hostname:~$ if [ $(echo "${missing_fields_lines[@]}" | wc -w) -eq 0 ]; then
remote@hostname:~$     echo "Нет строк с отсутствующими полями."
remote@hostname:~$ else
remote@hostname:~$     printf '%s\n' "${missing_fields_lines[@]}"
remote@hostname:~$ fi
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Ошибки в логах"
remote@hostname:~$ echo
remote@hostname:~$ # grep -h "ERROR" "$LOG_DIR"/*.log | tail -n 10
```
---
```
#### Сводка событий DELETE
**Диапазон дат:** 2024-07-20 23:28:57 MSK postgres до 2024-09-19 21:47:13 MSK postgres
**Всего событий:** 1472
**Процент успешных:** 100% (1472 успешных, 0 неудачных)
#### Подробная разбивка
1. **Временная шкала событий**
   - Первое событие: 2024-07-20 23:28:57 MSK postgres
   - Последнее событие: 2024-09-19 21:47:13 MSK postgres
2. **Идентификаторы субъектов**
   - database: 1472
#### Анализ
- **Согласованность:** 100% операций DELETE были успешными.
- **Частота:** События происходили в течение периода от первого до последнего события.
- **Использование базы данных:** Операции DELETE выполнялись на 1 базе(ах) данных.
#### Проверка обязательной информации
Дата и время: Да (Количество: 1472)
Идентификатор процесса: Да (Количество: 1472)
Идентификатор субъекта: Да (Количество: 1472)
Результат выполнения: Да (Количество: 1472)
Всего Да: 4
Всего Нет: 0
Все обязательные поля присутствуют: Да
#### Строки с отсутствующими полями
2024-07-20 23:28:57 MSK postgres database - Missing: Date/Time ProcessID SubjectID ExecutionResult 
#### Ошибки в логах
```
# Анализ событий модификации таблицы - UPDATE
```console
remote@hostname:~$ #!/bin bash
remote@hostname:~$ LOG_DIR="/var/lib/postgresql/16/main/log"
remote@hostname:~$ # Инициализация переменных
remote@hostname:~$ total_events=0
remote@hostname:~$ success_events=0
remote@hostname:~$ failure_events=0
remote@hostname:~$ first_event=""
remote@hostname:~$ last_event=""
remote@hostname:~$ declare -A subject_ids
remote@hostname:~$ # Инициализация счетчиков для обязательных полей
remote@hostname:~$ date_time_count=0
remote@hostname:~$ process_id_count=0
remote@hostname:~$ subject_id_count=0
remote@hostname:~$ execution_result_count=0
remote@hostname:~$ # Массив для хранения строк с отсутствующими полями
remote@hostname:~$ missing_fields_lines=()
remote@hostname:~$ # Обработка лог-файлов
remote@hostname:~$ while IFS=',' read -r date time db pid _ _ _ _ _ result rest; do
remote@hostname:~$     if [[ "$rest" == *"statement: UPDATE FROM table"* ]]; then
remote@hostname:~$         ((total_events++))
remote@hostname:~$         
remote@hostname:~$         event_datetime="$date $time"
remote@hostname:~$         
remote@hostname:~$         if [[ -z "$first_event" || "$event_datetime" < "$first_event" ]]; then
remote@hostname:~$             first_event="$event_datetime"
remote@hostname:~$         fi
remote@hostname:~$         
remote@hostname:~$         if [[ -z "$last_event" || "$event_datetime" > "$last_event" ]]; then
remote@hostname:~$             last_event="$event_datetime"
remote@hostname:~$         fi
remote@hostname:~$         
remote@hostname:~$         if [[ "$result" == "LOG" || "$result" == "0" || -z "$result" ]]; then
remote@hostname:~$             ((success_events++))
remote@hostname:~$         else
remote@hostname:~$             ((failure_events++))
remote@hostname:~$         fi
remote@hostname:~$         
remote@hostname:~$         # Increment the count for this database
remote@hostname:~$         if [[ -n "$db" ]]; then
remote@hostname:~$             subject_ids["$db"]=$((${subject_ids["$db"]:=0} + 1))
remote@hostname:~$         fi
remote@hostname:~$         # Проверка каждого обязательного поля
remote@hostname:~$         missing_fields=""
remote@hostname:~$         [[ "$date" != "" && "$time" != "" ]] && ((date_time_count++)) || missing_fields+="Date/Time "
remote@hostname:~$         [[ "$pid" != "" ]] && ((process_id_count++)) || missing_fields+="ProcessID "
remote@hostname:~$         [[ "$db" != "" ]] && ((subject_id_count++)) || missing_fields+="SubjectID "
remote@hostname:~$         [[ "$result" != "" ]] && ((execution_result_count++)) || missing_fields+="ExecutionResult "
remote@hostname:~$         # Если есть отсутствующие поля, добавляем строку в массив
remote@hostname:~$         log_line="$date $time $db"
remote@hostname:~$         if [[ -n "$missing_fields" ]]; then
remote@hostname:~$             missing_fields_lines+=("$log_line - Missing: $missing_fields")
remote@hostname:~$         fi
remote@hostname:~$     fi
remote@hostname:~$ done < <(grep -h "statement: UPDATE FROM table" "$LOG_DIR"/*.log)
remote@hostname:~$ # Вывод результатов
remote@hostname:~$ echo "#### Сводка событий UPDATE"
remote@hostname:~$ echo
remote@hostname:~$ echo "**Диапазон дат:** $first_event до $last_event"
remote@hostname:~$ echo "**Всего событий:** $total_events"
remote@hostname:~$ if [ $total_events -eq 0 ]; then
remote@hostname:~$     echo "**Процент успешных:** N/A (0 успешных, 0 неудачных)"
remote@hostname:~$ else
remote@hostname:~$     echo "**Процент успешных:** $((success_events * 100 / total_events))% ($success_events успешных, $failure_events неудачных)"
remote@hostname:~$ fi
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Подробная разбивка"
remote@hostname:~$ echo
remote@hostname:~$ echo "1. **Временная шкала событий**"
remote@hostname:~$ echo "   - Первое событие: $first_event"
remote@hostname:~$ echo "   - Последнее событие: $last_event"
remote@hostname:~$ echo
remote@hostname:~$ echo "2. **Идентификаторы субъектов**"
remote@hostname:~$ for db in "${!subject_ids[@]}"; do
remote@hostname:~$     echo "   - $db: ${subject_ids[$db]}"
remote@hostname:~$ done
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Анализ"
remote@hostname:~$ echo
remote@hostname:~$ if [ $total_events -eq 0 ]; then
remote@hostname:~$     echo "- **Согласованность:** N/A (нет событий)"
remote@hostname:~$ else
remote@hostname:~$     echo "- **Согласованность:** $((success_events * 100 / total_events))% операций UPDATE были успешными."
remote@hostname:~$ fi
remote@hostname:~$ echo "- **Частота:** События происходили в течение периода от первого до последнего события."
remote@hostname:~$ echo "- **Использование базы данных:** Операции UPDATE выполнялись на $(echo "${!subject_ids[@]}" | wc -w) базе(ах) данных."
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Проверка обязательной информации"
remote@hostname:~$ mandatory_fields=0
remote@hostname:~$ mandatory_fields_present=0
remote@hostname:~$ [[ $date_time_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ [[ $process_id_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ [[ $subject_id_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ [[ $execution_result_count -eq $total_events ]] && ((mandatory_fields_present++))
remote@hostname:~$ echo "Дата и время: $([ $date_time_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $date_time_count)"
remote@hostname:~$ echo "Идентификатор процесса: $([ $process_id_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $process_id_count)"
remote@hostname:~$ echo "Идентификатор субъекта: $([ $subject_id_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $subject_id_count)"
remote@hostname:~$ echo "Результат выполнения: $([ $execution_result_count -eq $total_events ] && echo "Да" || echo "Нет") (Количество: $execution_result_count)"
remote@hostname:~$ echo
remote@hostname:~$ mandatory_fields=4
remote@hostname:~$ echo "Всего Да: $mandatory_fields_present"
remote@hostname:~$ echo "Всего Нет: $((mandatory_fields - mandatory_fields_present))"
remote@hostname:~$ echo
remote@hostname:~$ echo "Все обязательные поля присутствуют: $([ $mandatory_fields_present -eq $mandatory_fields ] && echo "Да" || echo "Нет")"
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Строки с отсутствующими полями"
remote@hostname:~$ if [ $(echo "${missing_fields_lines[@]}" | wc -w) -eq 0 ]; then
remote@hostname:~$     echo "Нет строк с отсутствующими полями."
remote@hostname:~$ else
remote@hostname:~$     printf '%s\n' "${missing_fields_lines[@]}"
remote@hostname:~$ fi
remote@hostname:~$ echo
remote@hostname:~$ echo "#### Ошибки в логах"
remote@hostname:~$ echo
remote@hostname:~$ # grep -h "ERROR" "$LOG_DIR"/*.log | tail -n 10
```
---
```
#### Сводка событий UPDATE
**Диапазон дат:** 2024-07-20 22:55:35 MSK postgres до 2024-09-19 21:36:21 MSK postgres
**Всего событий:** 1432
**Процент успешных:** 100% (1432 успешных, 0 неудачных)
#### Подробная разбивка
1. **Временная шкала событий**
   - Первое событие: 2024-07-20 22:55:35 MSK postgres
   - Последнее событие: 2024-09-19 21:36:21 MSK postgres
2. **Идентификаторы субъектов**
   - database: 1432
#### Анализ
- **Согласованность:** 100% операций UPDATE были успешными.
- **Частота:** События происходили в течение периода от первого до последнего события.
- **Использование базы данных:** Операции UPDATE выполнялись на 1 базе(ах) данных.
#### Проверка обязательной информации
Дата и время: Да (Количество: 1432)
Идентификатор процесса: Да (Количество: 1432)
Идентификатор субъекта: Да (Количество: 1432)
Результат выполнения: Да (Количество: 1432)
Всего Да: 4
Всего Нет: 0
Все обязательные поля присутствуют: Да
#### Строки с отсутствующими полями
2024-07-21 07:28:05 MSK postgres database - Missing: Date/Time ProcessID SubjectID ExecutionResult 
#### Ошибки в логах
```
 