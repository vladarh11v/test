# ПУД 1 | Список УЗ PostrgeSQL | вывод команды - echo $(sudo -u postgres psql -t -P format=unaligned -c 'select  usename from pg_user')
```console
remote@hostname:~$ echo $(sudo -u postgres psql -t -P format=unaligned -c 'select  usename from pg_user') | tr ' ' '
' | sort -n
```
---
```
bob
davide
postgres
```
# Сохранение эталонного списка УЗ PostrgeSQL | вывод команды - sort -n "/home/user/Downloads/psql-auto-demo-test-main/files/tmp/psql_users" # выгрузка эталонного списка УЗ
```console
localhost@hostname:~$ sort -n "/home/user/Downloads/psql-auto-demo-test-main/files/tmp/psql_users" # выгрузка эталонного списка УЗ
```
---
```
postgres
```
# Группы не содержащиеся в эталонном списке ad_users
```console
localhost@hostname:~$ comm -13 <(echo -e "postgres") <(echo -e "bob\ndavide\npostgres")
```
---
```
bob
davide
```
 